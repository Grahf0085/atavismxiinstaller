import { defineConfig } from 'astro/config'
/* import basicSsl from '@vitejs/plugin-basic-ssl' */

import solidJs from '@astrojs/solid-js'

export default defineConfig({
  integrations: [solidJs()],
  /* vite: { */
  /*   plugins: [basicSsl()], */
  /*   server: { */
  /*     https: true, */
  /*   }, */
  /* }, */
})
