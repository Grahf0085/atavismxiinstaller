import { createSignal, onMount } from 'solid-js'
import FaceSelection from './FaceSelection'
import RaceSelection from './RaceSelection'
import SizeSelection from './SizeSelection'
import JobSelection from './JobSelection'
import NationSelection from './NationSelection'
import CreationSubmission from './CreationSubmission'
import Animate from './Animate'
import { getPlayer } from '../../utils/auth'
import { races } from '../../utils/faces'
import '../../styles/creation/charCreation.css'

export default function AdventurerCreation() {
  const [step, setStep] = createSignal(0)
  const [selectedRace, setSelectedRace] = createSignal('hume')
  const [playerId, setPlayerId] = createSignal()

  const facesToShow = () => races[selectedRace()]

  const [selectedFace, setSelectedFace] = createSignal(facesToShow()[0])
  const [selectedSize, setSelectedSize] = createSignal(0)
  const [selectedJob, setSelectedJob] = createSignal(1)
  const [selectedNation, setSelectedNation] = createSignal(0)

  onMount(async () => {
    const player = await getPlayer()
    setPlayerId(player.id)
  })

  return (
    <>
      <Animate
        showWhen={step() === 0}
        component={
          <RaceSelection
            selectedRace={selectedRace()}
            setSelectedRace={setSelectedRace}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 1}
        component={
          <FaceSelection
            selectedRace={selectedRace()}
            selectedFace={selectedFace()}
            setSelectedFace={setSelectedFace}
            facesToShow={facesToShow()}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 2}
        component={
          <SizeSelection
            selectedRace={selectedRace()}
            selectedFace={selectedFace().substring(0, 2)}
            selectedSize={selectedSize()}
            setSelectedSize={setSelectedSize}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 3}
        component={
          <JobSelection
            selectedJob={selectedJob()}
            setSelectedJob={setSelectedJob}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 4}
        component={
          <NationSelection
            selectedNation={selectedNation()}
            setSelectedNation={setSelectedNation}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 5}
        component={
          <CreationSubmission
            accountId={playerId()}
            selectedRace={selectedRace()}
            selectedFace={selectedFace()}
            selectedSize={selectedSize()}
            selectedJob={selectedJob()}
            selectedNation={selectedNation()}
            setStep={setStep}
          />
        }
      />
    </>
  )
}
