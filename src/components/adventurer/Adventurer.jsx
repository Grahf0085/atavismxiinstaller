import { createResource } from 'solid-js'
import { fetchAdventurerDetails } from '../../utils/adventurerDetails'
import '../../styles/adventurer/adventurer.css'

export default function Adventurer(props) {
  const [fetchedDetails] = createResource(
    props.charname,
    fetchAdventurerDetails,
  )

  return (
    <a
      class='detailsLink'
      href={`${props.charname ? `/adventurers/` : '/player/create-adventurer'}`}
    >
      <figure class='adventurerFigure'>
        <figcaption>
          <b>{props.charname || 'Comer'}</b>
        </figcaption>
        <Show
          when={fetchedDetails()?.fetchedImage}
          fallback={
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='80'
              height='80'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              stroke-width='2'
              stroke-linecap='round'
              stroke-linejoin='round'
              class='lucide lucide-help-circle'
            >
              <circle cx='12' cy='12' r='10' />
              <path d='M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3' />
              <path d='M12 17h.01' />
            </svg>
          }
        >
          <img
            class='advImage'
            src={fetchedDetails()?.fetchedImage}
            alt={props.charname}
          />
        </Show>
        <span class='titleSpan'>
          <em>the</em> {fetchedDetails()?.title || 'future adventurer'}
        </span>
      </figure>

      <Show
        when={fetchedDetails()?.subJob}
        fallback={
          <div>
            {fetchedDetails()?.mainJob}
            {props.mlvl}
          </div>
        }
      >
        <div>
          {fetchedDetails()?.mainJob}
          {props.mlvl} /<br /> {fetchedDetails()?.subJob}
          {props.slvl}
        </div>
      </Show>
      <div class='adventurerLocation'>{fetchedDetails()?.translatedZone}</div>
    </a>
  )
}
