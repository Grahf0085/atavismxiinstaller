import { checkUpdate, installUpdate } from '@tauri-apps/api/updater'
import { Show, createSignal, onMount } from 'solid-js'

export default function Update() {
  const [update, setUpdate] = createSignal()

  onMount(async () => {
    const update = await checkUpdate()
    setUpdate(update)
  })

  const handleUpdate = async () => {
    console.log(
      `Installing update ${update.manifest?.version}, ${update.manifest?.date}, ${update.manifest.body}`,
    )
    await installUpdate()
  }

  return (
    <Show when={update()?.shouldUpdate}>
      <button onClick={handleUpdate}>Update</button>
    </Show>
  )
}
