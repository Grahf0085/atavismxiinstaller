import { Store } from 'tauri-plugin-store-api'
import { invoke } from '@tauri-apps/api/tauri'
import { exists } from '@tauri-apps/api/fs'
import { createSignal, onCleanup, onMount } from 'solid-js'
import { GAME_FOLDER } from '../../utils/consts'
import '../../styles/play.css'

export default function Play(props) {
  let cliWatcher

  const store = new Store('.settings.dat')

  const [isWineInstalled, setIsWineInstalled] = createSignal(false)
  const [cliExists, setCliExists] = createSignal(false)
  const [watcher, setWatcher] = createSignal(null)
  const [currentOs, setCurrentOs] = createSignal()

  const loadWatcher = async () => {
    const module = await import('tauri-plugin-fs-watch-api')
    setWatcher(module)
  }

  const loadOs = async () => {
    const { type } = await import('@tauri-apps/api/os')
    const currentOs = await type()
    setCurrentOs(currentOs)
  }

  const checkForWine = async () => {
    const isWineInstalled = await invoke('is_wine_installed')
    setIsWineInstalled(isWineInstalled)
  }

  const runWine = async () => {
    if (isWineInstalled()) {
      const playerName = props.playerName
      const folderWithCli = (await store.get('atavismxi-dir')) + GAME_FOLDER

      await invoke('run_wine', {
        installedDir: folderWithCli,
        playerName,
      })
    }
  }

  const checkForCli = async (cliPath) => {
    const cliExists = await exists(cliPath)
    setCliExists(cliExists)
  }

  onMount(async () => {
    await loadWatcher()
    await loadOs()

    const installedDir = await store.get('atavismxi-dir')

    await checkForWine()

    checkForCli(installedDir + GAME_FOLDER + '/Ashita-cli.exe')

    try {
      cliWatcher = await watcher().watch(
        installedDir + GAME_FOLDER + '/Ashita-cli.exe',
        async (event) => {
          checkForCli(installedDir + GAME_FOLDER + '/Ashita-cli.exe')
        },
        { recursive: true },
      )
    } catch (error) {
      setCliExists(false)
    }
  })

  onCleanup(() => {
    //not sure if this works, ie unwatches
    cliWatcher
  })

  return (
    <>
      <Show when={isWineInstalled() && cliExists() && currentOs() === 'Linux'}>
        <button
          onClick={runWine}
          disabled={!isWineInstalled() || !cliExists()}
          class='playButton'
        >
          Play Atavism XI
        </button>
      </Show>
      <Show when={!isWineInstalled() && currentOs() === 'Linux'}>
        <button class='playButton' onClick={checkForWine}>
          Check For Wine Installation
        </button>
      </Show>
    </>
  )
}
