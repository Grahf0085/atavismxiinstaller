import { createSignal, For } from 'solid-js'
import { Select } from '@thisbeyond/solid-select'
import '@thisbeyond/solid-select/style.css'
import '../styles/ruleList.css'

export default function RuleList(props) {
  const [selectedRules, setSelectedRules] = createSignal(props.general)

  const options = [
    { value: JSON.stringify(props.general), label: 'General' },
    { value: JSON.stringify(props.ashitaApproved), label: 'Approved Ashita' },
    { value: JSON.stringify(props.ashitaBanned), label: 'Banned Ashita' },
    {
      value: JSON.stringify(props.windowerApproved),
      label: 'Approved Windower',
    },
    { value: JSON.stringify(props.windowerBanned), label: 'Banned Windower' },
  ]

  const handleChange = (e) => {
    const selectedValue = e.value
    const selectedArray = JSON.parse(selectedValue)
    setSelectedRules(selectedArray)
  }

  return (
    <div class='rulesContainer'>
      <label for='ruleSelect' class='ruleLabel'>
        Select a Rule Set:
      </label>
      <Select
        onChange={(e) => handleChange(e)}
        class='custom'
        options={options}
        format={(item) => item.label}
        placeholder='General'
        id='ruleSelect'
      />
      <ol class='listContainer'>
        <For each={selectedRules()}>
          {(item) => <li class='rulesItem'>{item}</li>}
        </For>
      </ol>
    </div>
  )
}
