import {
  jobConverter,
  formatAdventurer,
  adventurerImage,
  zones,
} from './adventurerConversion'

import bastokEmblem from '../media/nations/bastok.webp'
import sandoriaEmblem from '../media/nations/sandoria.webp'
import windurstEmblem from '../media/nations/windurst.webp'

import fishingEmblem from '../media/crafting/fish.webp'
import woodworkingEmblem from '../media/crafting/wood.webp'
import smithingEmblem from '../media/crafting/smith.webp'
import goldsmithingEmblem from '../media/crafting/gold.webp'
import weavingEmblem from '../media/crafting/weave.webp'
import leathercraftEmblem from '../media/crafting/leather.webp'
import bonecraftEmblem from '../media/crafting/bone.webp'
import alchemyEmblem from '../media/crafting/alchemy.webp'
import cookingEmblem from '../media/crafting/cook.webp'
import diggingEmblem from '../media/crafting/dig.webp'

import { titles } from './titles.js'

export const nationEmblems = [
  { pic: sandoriaEmblem, alt: 'Emblem of Sandoria' },
  { pic: bastokEmblem, alt: 'Emblem of Bastok' },
  { pic: windurstEmblem, alt: 'Emblem of Windurst' },
]

const emptyAdventurer = {
  accountId: '',
  charId: '',
  charName: '',
  mainLevel: '',
  subLevel: '',
  mainJob: '',
  subJob: '',
  fetchedImage: '',
  title: '',
  nations: '',
  jobs: '',
  crafts: '',
  translatedZone: '',
}

export const fetchAdventurerDetails = async (name) => {
  if (!name) return emptyAdventurer
  try {
    const response = await fetch(
      `https://www.atavismxi.com/api/adventurer/${name}`,
    )

    if (!response.ok) {
      throw await response.json()
    }
    const allAdventurerDetails = await response.json()

    /* I think this should always be the right adventurer */
    const adventurerDetails = allAdventurerDetails[0]

    const accountId = adventurerDetails?.accid
    const charId = adventurerDetails?.charid
    const charName = adventurerDetails?.charname
    const mainLevel = adventurerDetails?.mlvl
    const subLevel = adventurerDetails?.slvl
    const mainJob = jobConverter(adventurerDetails?.mjob)
    const subJob = jobConverter(adventurerDetails?.sjob)

    const formattedAdventurer = formatAdventurer(
      adventurerDetails?.race,
      adventurerDetails?.face,
    )

    const fetchedImage = adventurerImage(formattedAdventurer)
    const title = titles[adventurerDetails?.title]
    const translatedZone = zones[adventurerDetails?.pos_zone]

    const nations = [
      {
        rank: adventurerDetails?.rankSandoria,
        pic: nationEmblems[0].pic.src,
        alt: nationEmblems[0].alt,
      },
      {
        rank: adventurerDetails?.rankBastok,
        pic: nationEmblems[1].pic.src,
        alt: nationEmblems[1].alt,
      },
      {
        rank: adventurerDetails?.rankWindurst,
        pic: nationEmblems[2].pic.src,
        alt: nationEmblems[2].alt,
      },
    ]

    const jobs = [
      {
        job: 'WAR',
        level: adventurerDetails?.war,
      },
      {
        job: 'MNK',
        level: adventurerDetails?.mnk,
      },
      {
        job: 'WHM',
        level: adventurerDetails?.whm,
      },
      {
        job: 'BLM',
        level: adventurerDetails?.blm,
      },
      {
        job: 'RDM',
        level: adventurerDetails?.rdm,
      },
      {
        job: 'THF',
        level: adventurerDetails?.thf,
      },
      {
        job: 'PLD',
        level: adventurerDetails?.pld,
      },
      {
        job: 'DRK',
        level: adventurerDetails?.drk,
      },
      {
        job: 'BST',
        level: adventurerDetails?.bst,
      },
      {
        job: 'BRD',
        level: adventurerDetails?.brd,
      },
      {
        job: 'RNG',
        level: adventurerDetails?.rng,
      },
      {
        job: 'SAM',
        level: adventurerDetails?.sam,
      },
      {
        job: 'NIN',
        level: adventurerDetails?.nin,
      },
      {
        job: 'DRG',
        level: adventurerDetails?.drg,
      },
      {
        job: 'SMN',
        level: adventurerDetails?.smn,
      },
    ]

    const crafts = [
      {
        title: 'Fishing',
        pic: fishingEmblem.src,
        alt: 'Fishing Guild Emblem',
        level: adventurerDetails?.fishing || 0,
      },
      {
        title: 'Woodworking',
        pic: woodworkingEmblem.src,
        alt: 'Woodworking Guild Emblem',
        level: adventurerDetails?.woodworking || 0,
      },
      {
        title: 'Smithing',
        pic: smithingEmblem.src,
        alt: 'Smithing Guild Emblem',
        level: adventurerDetails?.smithing || 0,
      },
      {
        title: 'Goldsmithing',
        pic: goldsmithingEmblem.src,
        alt: 'Goldsmithing Guild Emblem',
        level: adventurerDetails?.goldsmithing || 0,
      },
      {
        title: 'Clothcraft',
        pic: weavingEmblem.src,
        alt: 'Clothcraft Guild Emblem',
        level: adventurerDetails?.weaving || 0,
      },
      {
        title: 'Leathercraft',
        pic: leathercraftEmblem.src,
        alt: 'Leathercraft Guild Emblem',
        level: adventurerDetails?.leathercraft || 0,
      },
      {
        title: 'Bonecraft',
        pic: bonecraftEmblem.src,
        alt: 'Bonecraft Guild Emblem',
        level: adventurerDetails?.bonecraft || 0,
      },
      {
        title: 'Alchemy',
        pic: alchemyEmblem.src,
        alt: 'Alchemy Guild Emblem',
        level: adventurerDetails?.alchemy || 0,
      },
      {
        title: 'Cooking',
        pic: cookingEmblem.src,
        alt: 'Cooking Guild Emblem',
        level: adventurerDetails?.cooking || 0,
      },
      {
        title: 'Digging',
        pic: diggingEmblem.src,
        alt: 'Digging Guild Emblem',
        level: adventurerDetails?.digging || 0,
      },
    ]

    return {
      accountId,
      charId,
      charName,
      mainLevel,
      subLevel,
      mainJob,
      subJob,
      fetchedImage,
      title,
      nations,
      jobs,
      crafts,
      translatedZone,
    }
  } catch (error) {
    console.error('Error Fetching Adventurer Details: ', error)
    return emptyAdventurer
  }
}
